from collections import defaultdict, Counter
from captions import Parser
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.stem import WordNetLemmatizer
import matplotlib.pyplot as plt
import pandas as pd
import nltk
import numpy as np

WORDS = 100

# First time only
# import nltk
# nltk.download('all')

tokens = []

pos_tags = nltk.pos_tag(tokens)


def pos_tagger(nltk_tag):
    if nltk_tag.startswith("J"):
        return nltk.corpus.wordnet.ADJ
    elif nltk_tag.startswith("V"):
        return nltk.corpus.wordnet.VERB
    elif nltk_tag.startswith("N"):
        return nltk.corpus.wordnet.NOUN
    elif nltk_tag.startswith("R"):
        return nltk.corpus.wordnet.ADV
    else:
        return None


def getSentiment(text):
    analyzer = SentimentIntensityAnalyzer()
    scores = analyzer.polarity_scores(text)
    return scores

def plotter(folder):
    print(f"plotting for {folder}")
    p = Parser()
    p.parse(folder, cut_off_date="2012-01-01")

    text_by_year = defaultdict(list)

    print("# Raw Scatter Plot:")
    print(
        "videoId, words-per-min, viewCount, publishedAt, negative, neutral, positive, compound"
    )
    for videoId, first_ref in p.first_word_in_video.items():
        ref = first_ref
        year = int(ref.publishedAt.split("-")[0])
        word_counter = 0
        end = 0
        text = []
        while ref:
            word_counter += 1
            end = ref.start + ref.duration
            text_by_year[year].append(ref.word)
            text.append(ref.word)
            # dictionary[ref.word] += 1
            tokens.append(ref.word)
            ref = ref.next_ref

        rate = word_counter / (end / 60.0)
        stats = p.video_stats[videoId]
        sentiment = getSentiment("\n".join(text))
        print(
            "%s %.2f %s %s %g %g %g %g"
            % (
                videoId,
                rate,
                stats["viewCount"],
                first_ref.publishedAt,
                sentiment["neg"],
                sentiment["neu"],
                sentiment["pos"],
                sentiment["compound"],
            )
        )
        # print '%s: %d words %.2f min %.2f words/min views: %s' % (videoId, word_counter, end/60.0, rate, stats)

    print()
    print("# Year over Year sentiment:")
    print("year, negative, neutral, positive, compound")
    for year, text in sorted(text_by_year.items()):
        sentiment = getSentiment("\n".join(text))
        print(
            "%s %g %g %g %g"
            % (
                year,
                sentiment["neg"],
                sentiment["neu"],
                sentiment["pos"],
                sentiment["compound"],
            )
        )

    pos_tags = nltk.pos_tag(tokens)

    tmp = []
    for word, tag in pos_tags:
        # pirnt only unique tags
        if tag not in tmp:
            tmp.append(tag)
    print("Unique POS tags: ", tmp)

    wordnet_tagged = list(map(lambda x: (x[0], pos_tagger(x[1])), pos_tags))


    lemmatizer = WordNetLemmatizer()

    token_list = []
    for word, tag in wordnet_tagged:
        if tag is None:
            token_list.append(word)
        else:
            token_list.append(lemmatizer.lemmatize(word, tag))

    cnt = Counter()

    for token in token_list:
        cnt[token] += 1

    fig, ax = plt.subplots(figsize=(12, 8))

    all_word_freq = pd.DataFrame(cnt.most_common(WORDS), columns=["words", "count"])
    all_word_freq.head()
    all_word_freq.sort_values(by="count").plot.barh(x="words", y="count", ax=ax, color="brown")
    for i in ax.patches:
        ax.text(
            i.get_width() + 0.2, i.get_y() + 0.1, str(round(i.get_width(), 2)), fontsize=10
        )
    ax.set_title("Common Words Found")
    plt.savefig(f"{folder}-word-freq-bar.png")

    all_word_freq["log_count"] = all_word_freq["count"].apply(lambda x: np.log(x))
    all_word_freq["log_rank"] = all_word_freq["count"].apply(
        lambda x: np.log(all_word_freq[all_word_freq["count"] >= x].count()[0])
    )

    all_word_freq = all_word_freq.sort_values(by="log_rank")

    fig, ax = plt.subplots(figsize=(12, 8))
    all_word_freq.plot.line(x="log_rank", y="log_count", ax=ax)
    ax.set_title("Log-log plot of word frequency")
    ax.set_xlabel("Log Rank")
    ax.set_ylabel("Log Count")
    plt.savefig(f"{folder}-word-freq.png")

    neg = Counter()
    neu = Counter()
    pos = Counter()

    for token in cnt.keys():
        sentiment = getSentiment(token)
        if sentiment["neg"] > sentiment["neu"] and sentiment["neg"] > sentiment["pos"]:
            neg[token] = cnt[token]
        elif sentiment["neu"] > sentiment["neg"] and sentiment["neu"] > sentiment["pos"]:
            neu[token] = cnt[token]
        else:
            pos[token] = cnt[token]

    word_freq1 = pd.DataFrame(neg.most_common(WORDS), columns=["words", "count"])
    word_freq2 = pd.DataFrame(neu.most_common(WORDS), columns=["words", "count"])
    word_freq3 = pd.DataFrame(pos.most_common(WORDS), columns=["words", "count"])

    for curr_word_freq in [word_freq1, word_freq2, word_freq3]:
        curr_word_freq["log_count"] = curr_word_freq["count"].apply(lambda x: np.log(x))
        curr_word_freq["log_rank"] = curr_word_freq["count"].apply(
            lambda x: np.log(curr_word_freq[curr_word_freq["count"] >= x].count()[0])
        )
        curr_word_freq.sort_values(by="log_rank", inplace=True)

    fig, ax = plt.subplots(figsize=(12, 8))
    word_freq1.plot.line(x="log_rank", y="log_count", ax=ax, color="blue", label="neg")
    word_freq2.plot.line(x="log_rank", y="log_count", ax=ax, color="green", label="neu")
    word_freq3.plot.line(x="log_rank", y="log_count", ax=ax, color="red", label="pos")
    all_word_freq.plot.line(x="log_rank", y="log_count", ax=ax, color="black", label="all words")

    ax.set_title("Log-log plot of word frequency for three sets")
    ax.set_xlabel("Log Rank")
    ax.set_ylabel("Log Count")
    ax.legend()
    plt.savefig(f"{folder}-word-freq-sentiment.png")

plotter("primeagen")
plotter("tsoding")
plotter("tj_devries")
plotter("fireship")
